package main

import (
	"bufio"
	"fmt"
	"os/exec"
	"strconv"
	"strings"
)

type cmusData struct {
	status       string
	duration     string
	durationSecs int
	position     string
	positionSecs int
	title        string
	artist       string
}

func parseCmus() cmusData {
	cmd := exec.Command("cmus-remote", "-Q")

	stdout, err := cmd.StdoutPipe()
	if err != nil {
		panic(err)
	}

	if err := cmd.Start(); err != nil {
		panic(err)
	}

	scanner := bufio.NewScanner(stdout)

	data := cmusData{}

	for scanner.Scan() {
		line := scanner.Text()

		if strings.Contains(line, "status") && data.status == "" {
			data.status = strings.SplitN(line, " ", 3)[1]
		}
		if strings.Contains(line, "duration") && data.duration == "" {
			durationSecsStr := strings.Split(line, " ")[1]
			if durationSecs, err := strconv.Atoi(durationSecsStr); err == nil {
				data.durationSecs = durationSecs
			}

			data.duration = getTimeElapsed(strings.SplitN(line, " ", 3)[1])
		}
		if strings.Contains(line, "position") && data.position == "" {
			positionSecsStr := strings.Split(line, " ")[1]
			if positionSecs, err := strconv.Atoi(positionSecsStr); err == nil {
				data.positionSecs = positionSecs
			}
			data.position = getTimeElapsed(strings.SplitN(line, " ", 3)[1])
		}
		if strings.Contains(line, "title") && data.title == "" {
			data.title = strings.SplitN(line, " ", 3)[2]
		}
		if strings.Contains(line, "artist") && data.artist == "" {
			data.artist = strings.SplitN(line, " ", 3)[2]
		}
	}

	data.durationSecs -= data.positionSecs

	return data
}

func getTimeElapsed(duration string) string {
	outputDuration, _ := strconv.Atoi(duration)

	durationLeftoverSeconds := outputDuration % 60
	durationMinutes := (outputDuration - durationLeftoverSeconds) / 60

	secsPrefix := "0"
	if durationLeftoverSeconds > 9 {
		secsPrefix = ""
	}

	return fmt.Sprintf("%v:%v%v", durationMinutes, secsPrefix, durationLeftoverSeconds)
}
