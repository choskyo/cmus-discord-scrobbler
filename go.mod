module cdrpc

go 1.15

require (
	github.com/hugolgst/rich-go v0.0.0-20210525072106-9d45f0e06959
	gopkg.in/natefinch/npipe.v2 v2.0.0-20160621034901-c1b8fa8bdcce // indirect
)
