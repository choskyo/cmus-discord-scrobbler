package main

import (
	"strings"
	"time"

	"github.com/hugolgst/rich-go/client"
)

func main() {
	err := client.Login("870067151615197215")
	if err != nil {
		panic(err)
	}

	ticker := time.NewTicker(1 * time.Second)

	for range ticker.C {
		updateStatus()
	}
}

func updateStatus() {
	data := parseCmus()

	status := strings.Trim(data.status, " ")

	if status != "playing" && status != "paused" {
		return
	}

	songStartTime := time.Now()
	songEndTime := time.Now().Add(time.Duration(data.durationSecs) * time.Second)

	err := client.SetActivity(client.Activity{
		State:   data.artist,
		Details: data.title,
		Timestamps: &client.Timestamps{
			Start: &songStartTime,
			End:   &songEndTime,
		},
	})

	if err != nil {
		panic(err)
	}
}
